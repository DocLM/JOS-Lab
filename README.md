# JOS LAB UNIMI
This repository is a copy of [6.828 class](https://pdos.csail.mit.edu/6.828/2014/) JOS repository.
It is configured using Gitlab-CI to automate the assessment of laboratories.

## LAB 1 GRADE STATUS
[![Build Status](https://gitlab.com/DocLM/JOS-Lab/badges/lab1/build.svg)](https://gitlab.com/DocLM/JOS-Lab/badges/lab1/build.svg)

## LAB 2 GRADE STATUS
[![Build Status](https://gitlab.com/DocLM/JOS-Lab/badges/lab2/build.svg)](https://gitlab.com/DocLM/JOS-Lab/badges/lab2/build.svg)

## LAB 3 GRADE STATUS
[![Build Status](https://gitlab.com/DocLM/JOS-Lab/badges/lab3/build.svg)](https://gitlab.com/DocLM/JOS-Lab/badges/lab3/build.svg)